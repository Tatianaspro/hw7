<?php

//
////Задача 1:
////Пусть в корне вашего сайта лежит файл test.txt.
////Считайте данные из этого файла и выведите их на экран.
//
////$file = "test.txt";
//if (file_exists($file)) {
//    $handle = fopen($file, "r") or die("ERROR: Cannot open the file.");
//    $content = fread($handle, "100");
//    fclose($handle);
//    echo $content.PHP_EOL;
//} else {
//    echo "ERROR: File does not exist.";
//}
//
//
////Задача 2:
////Пусть в корне вашего сайта лежит файл test.txt.
////Запишите в него текст '12345'.
//
////$file = "test.txt";
//$data = "12345";
//
//file_put_contents($file, $data) or die("ERROR: Cannot write the file.");
//
//echo "Data written to the file successfully.".PHP_EOL;
//
//
////Задача 3:
////Создайте файл test2.txt и запишите в него текст '12345'.
////Пусть изначально файла с таким именем не существует.
//
////$file = "test2.txt";
//
//if (!file_exists($file)) {
//    $handle = fopen($file, "a");
//    echo "File created.".PHP_EOL;
//} else {
//    echo "ERROR: File does not exist.";
//}
//
////Задача 4:
////Пусть в корне вашего сайта лежит файл test.txt, в котором записан текст '12345'.
////Откройте этот файл, запишите в конец текста восклицательный знак и сохраните новый текст обратно в файл.
//
////$file = "test.txt";
//$data = "!";
//
////file_put_contents($file, $data, FILE_APPEND) or die("ERROR: Cannot write the file.");
//
//echo "Data written to the file successfully.".PHP_EOL;
//
//
////Задача 5:
////Пусть в корне вашего сайта лежит файл count.txt.
////Изначально в нем записано число 0.
////Сделайте так, чтобы при обновлении страницы наш скрипт каждый раз увеличивал это число на 1.
////То есть у нас получится счетчик обновления страницы в виде файла.
//
////$file = "count.txt";
//
//    $content = file_get_contents($file) or die("ERROR: Cannot open the file.");
//    $content+=1;
//    echo $content;
//    $handle = fopen($file, "w") or die("ERROR: Cannot open the file.");
//
//fwrite($handle, $content) or die ("ERROR: Cannot write the file.");
//
//fclose($handle);
//
//echo "Data written to the file successfully.";
//
//
////Задача 6:
////Пусть в корне вашего сайта лежат файлы     1.txt, 2.txt и 3.txt.
////Вручную сделайте массив с именами этих файлов.
////Переберите его циклом, прочитайте содержимое каждого из файлов,
//// слейте его в одну строку и запишите в новый файл new.txt.
////Изначально этого файла не должно быть.
//
//$arr = ["1.txt","2.txt","3.txt"];
//$item = array();
// foreach ($arr as $value){
//     $file = $value;
//     $content = file_get_contents($file) or die("ERROR: Cannot open the file.");
//     $item[] = $content;
//      }
//
//
////Задача 7:
////Пусть в корне вашего сайта лежит файл old.txt.
////Переименуйте его на new.txt.
//
////$file = "old.txt";
//
//if (rename($file, "new.txt") == true) {
//    echo "File renamed successfully.";
//}
//
//
////Задача 8:
////Пусть в корне вашего сайта лежит файл test.txt.
////Пусть также в корне вашего сайта лежит папка dir.
////Переместите файл в эту папку.
//
////$file = "test.txt";
//
//if (rename($file, "dir/test.txt") == true) {
//    echo "File renamed successfully.";
//} else {
//    echo "ERROR: File cannot be renamed.";
//};
//
////Задача 9:
////Пусть в корне вашего сайта лежит файл test.txt.
////Скопируйте его в файл copy.txt.
//
////$file = "test.txt";
//
//if (file_exists($file)) {
//    if (copy($file, "copy.txt")) {
//        echo "File renamed successfully.";
//    } else {
//        echo "ERROR: File cannot be renamed.";
//    }
//} else {
//    echo "ERROR: File does not exist.";
//}
//
//
////Задача 10:
////Пусть в корне вашего сайта лежит файл test.txt.
////Удалите его.
//
////$file = "test.txt";
//
//if (file_exists($file)) {
//    if (unlink($file)) {
//        echo "File removed successfully.";
//    } else {
//        echo "ERROR: File cannot be removed.";
//    }
//} else {
//    echo "ERROR: File does not exist.";
//}
//
//
////Задача 11:
////Проверьте, лежит ли в корне вашего сайта файл test.txt.
//
//$file = 'dir/test.txt';
//
//if (file_exists($file)) {
//    echo "The file $file exists";
//} else {
//    echo "The file $file does not exist";
//}
//
////Задача 12:
////Пусть в корне вашего сайта лежит файл test.txt.
////Узнайте его размер, выведите на экран.
//
//$file = 'test.txt';
//echo 'Размер файла ' . $file . ': ' . filesize($file) . ' байтов';
//
//
////Задача 13:
////Положите в корень вашего сайта какую-нибудь картинку размером более мегабайта.
////Узнайте размер этого файла и переведите его в мегабайты.
//
//function Size2Str($size)
//{
//    $kb = 1024;
//    $mb = 1024 * $kb;
//    $gb = 1024 * $mb;
//    $tb = 1024 * $gb;
//
//    if ($size < $kb) {
//        return $size.' байт';
//    } else if ($size < $mb) {
//        return round($size / $kb, 2).' Кб';
//    } else if ($size < $gb) {
//        return round($size / $mb, 2).' Мб';
//    } else if ($size < $tb) {
//        return round($size / $gb, 2).' Гб';
//    } else {
//        return round($size / $tb, 2).' Тб';
//    }
//}
//$size = Size2Str(filesize("1.jpg"));
//echo 'The picture size is '."1.jpg".': '.$size;
//
////Задача 14:
////Дан файл test.txt.
////Прочитайте его текст, получите массив его строк.

//$file = "test.txt";
//$arr = file($file) or die("ERROR");
//foreach($arr as $line){
//    echo $line;
//}

////Задача 15:
////Дан файл test.txt.
////В нем на каждой строке написано какое-то число.
////С помощью функции file найдите сумму этих чисел и выведете ее на экран.
//
//$file = "test.txt";
//$result = array();
//$arr = file($file) or die ("ERROR");
//foreach ($arr as $line){
//    $result[] = $line;
//}
//
//echo array_sum($result);

//Задача 16:
//Создайте в корне вашего сайта папку с названием dir.

mkdir('dir1');
//
////Задача 17:
////Создайте в корне вашего сайта папку с названием test.
////Затем создайте в этой папке 3 файла: 1.txt, 2.txt, 3.txt.
//
//mkdir('test');
//$arr = [1,2,3];
//foreach ($arr as $value) {
//    $file = "test/$value.txt";
//    if (!file_exists($file)) {
//        $fp = fopen($file, "w");
//        fclose($fp);
//        echo $file;
//    }
//}
////
////Задача 18:
////Удалите папку с названием dir.
////
////rmdir('dir/*.txt');

////Задача 19:
////Пусть в корне вашего сайта лежит папка old.
////Переименуйте ее на new.
//
//rename("old", "new");

////Задача 20:
////Пусть в корне вашего сайта лежит папка dir, а в ней какие-то текстовые файлы.
////Переберите эти файлы циклом и выведите их тексты в браузер.
//
//$arr = scandir('test');
//foreach($arr as $value){
//    if(is_file($value)) {
//        $content = readfile($value) or die("ERROR: Cannot open the file.");
//        echo $content;
//    }
//}
//
////Задача 21:
////Пусть дан путь к файлу.
////Проверьте, файл это или папка.
//
//$way = "PhpstormProject/project/test/1";
//if(is_dir("$way")){
//    echo "It is folder";
//    } else {
//    echo "It is file";
//}

//Задача 22:
//Сделайте форму для загрузки 1 картинки.
//Напишите PHP код для валидации и загрузки картинки в отдельную папку в проекте.
?>
<form action="/index.php" method="post" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="submit" value="Отправить">
</form>

<?php
//Задача 23:
//Сделайте форму для загрузки нескольких картинок.
//Напишите PHP код для валидации и загрузки картинок в отдельную папку в проекте.
//
